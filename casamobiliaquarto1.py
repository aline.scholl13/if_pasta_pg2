from numpy import append


class Quarto:
    def __init__(self, nome, dimensoes):
        self.nome = nome
        self.dimensoes = dimensoes
    def __str__(self):
        return self.nome
        "," + self.dimensoes
class Mobilia:
    def __init__(self, nome, funcao, material, quarto):
        self.nome = nome
        self.funcao = funcao
        self.material = material
        self.quarto = quarto
    
    def __str__(self):
        s = self.nome + "," + self.funcao + "," + self.material
        if self.quarto:
            s += str(self.quarto)
            return s

class Casa:
    def __init__(self, formato, quartos):
        self.formato = formato
        if quartos is None:
            raise Exception("Casa precisa de quartos")
        self.quartos = quartos
    
    def __str__(self):
        s = f'Casa: {self.formato}'
        for quarto in self.quartos:
            s += f', possui: {str(quarto)}'
        return s

if __name__ == "__main__":

    q1 = Quarto(nome = "Sala", dimensoes = "10x12")
    q2 = Quarto(nome = "Quarto", dimensoes = "6x8")

    m1 = Mobilia(nome = "Cama", funcao = "Dormir nela", material = "Madeira(Mahogany)", quarto = q2)

    c1 = Casa(formato = "Cabana", quartos = [q1, q2])
print(m1)
print(c1)